<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBahanUntukTasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahan_untuk_tas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_tas');
            $table->unsignedBigInteger('id_bahan_baku');
            $table->double('qty');

            $table->foreign('id_tas')->references('id')->on('jenis_tas');
            $table->foreign('id_bahan_baku')->references('id')->on('jenis_bahan_baku');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bahan_untuk_tas');
    }
}
