<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatatanKeluarMasukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catatan_keluar_masuk', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_bahan_baku');
            $table->double('qty');
            $table->string('keluar_masuk');
            $table->string('keterangan')->nullable();
            $table->double('kumulatif');

            $table->foreign('id_bahan_baku')->references('id')->on('jenis_bahan_baku');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catatan_keluar_masuk');
    }
}
