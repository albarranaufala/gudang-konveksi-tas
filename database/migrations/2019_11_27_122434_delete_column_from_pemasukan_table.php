<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteColumnFromPemasukanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pemasukan', function (Blueprint $table) {
            $table->dropColumn('kumulatif');
        });
        Schema::table('pengeluaran', function (Blueprint $table) {
            $table->dropColumn('kumulatif');
        });
        Schema::table('catatan_keluar_masuk', function (Blueprint $table) {
            $table->dropColumn('kumulatif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pemasukan', function (Blueprint $table) {
            $table->double('kumulatif');
        });
        Schema::table('pengeluaran', function (Blueprint $table) {
            $table->double('kumulatif');
        });
        Schema::table('catatan_keluar_masuk', function (Blueprint $table) {
            $table->double('kumulatif');
        });
    }
}
