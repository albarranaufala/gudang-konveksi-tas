<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input Bahan Baku</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        html,
        body {
            height: 100%;
        }

        .full-height {
            height: 100%;
        }

        .center-page {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <div class="container">
                <a class="navbar-brand" href="/">Gudang Konveksi Tas</a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="/pemasukan/create">Input Bahan<span
                                class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tas">Spesifikasi Tas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/pengeluaran/create">Produksi Tas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/report">Report</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="full-height center-page">
        <div class="card col-md-6">
            <div class="card-body">
                <h4 class="mb-3"><strong>Input Bahan Baku</strong></h4>
                <div class="form-group">
                    <label for="jenisBahan">Jenis Bahan</label>
                    <div value="1" class="row d-flex" style="flex-direction:row-reverse">
                        <div class="col">
                            <button type="button" class="btn btn-link" data-toggle="modal"
                                data-target="#modalTambahJenisBahan">+ tambah jenis</button>
                            {{-- modal tambah jenis bahan baku --}}
                            <div class="modal fade" id="modalTambahJenisBahan" tabindex="-1" role="dialog"
                                aria-labelledby="modalTambahJenisBahanTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modalTambahJenisBahanTitle">Tambah Jenis Bahan
                                                Baku</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="/jenis_bahan_baku/store" method="POST">
                                            <div class="modal-body">
                                                {{-- isi modal --}}
                                                @csrf
                                                <div class="form-group">
                                                    <label for="namaBahan">Nama Bahan</label>
                                                    <input name="nama_bahan" type="text" class="form-control"
                                                        id="namaBahan" placeholder="cth:benang">
                                                </div>
                                                <div class="form-group">
                                                    <label for="unit">Unit (satuan)</label>
                                                    <input name="unit" type="text" class="form-control" id="unit"
                                                        placeholder="cth:meter">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Tambah</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <form action="/pemasukan/store" method="POST">
                                @csrf
                                <select name="id_bahan_baku" class="form-control" id="jenisBahan">
                                    @forelse($bahan_bakus as $bahan_baku)
                                    <option value="{{$bahan_baku->id}}">{{$bahan_baku->nama_bahan}}</option>
                                    @empty
                                    @endforelse
                                </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    @php
                    $month = date('m');
                    $day = date('d');
                    $year = date('Y');

                    $today = $year . '-' . $month . '-' . $day;
                    @endphp
                    <label for="tanggal">Tanggal Masuk</label>
                    <input name="tanggal" type="date" class="form-control" id="tanggal" value="<?php echo $today; ?>">
                </div>
                <div class="form-group">
                    <label for="qty">Kuantitas</label>
                    <input name="qty" step="any" type="number" class="form-control" id="qty" placeholder="Kuantitas">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <textarea name="keterangan" class="form-control" id="keterangan"
                        placeholder="Keterangan"></textarea>
                </div>
                <div class="form-row d-flex justify-content-between">
                    <a class="btn btn-link" href="/">kembali</a>
                    <button class="btn btn-primary" type="submit">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>

</html>