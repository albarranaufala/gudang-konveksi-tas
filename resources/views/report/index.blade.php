<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jenis Tas</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <div class="container">
                <a class="navbar-brand" href="/">Gudang Konveksi Tas</a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/pemasukan/create">Input Bahan<span
                                class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tas">Spesifikasi Tas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/pengeluaran/create">Produksi Tas</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/report">Report</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <h1 class="my-3"><strong>Catatan Keluar Masuk</strong></h1>
        <div class="row my-3">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary mx-1" data-toggle="modal" data-target="#sisaBahan">
                Sisa Bahan
            </button>
            <a href="/report/download_pdf" class="btn btn-success mx-1" style="color:white">Download PDF</a>
            <!-- Modal -->
            <div class="modal fade" id="sisaBahan" tabindex="-1" role="dialog" aria-labelledby="sisaBahanTitle"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="sisaBahanTitle">Sisa Bahan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nama Bahan</th>
                                        <th scope="col">Sisa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($bahan_bakus as $bahan_baku)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$bahan_baku->nama_bahan}}</td>
                                        <td>{{$bahan_baku->qty}} {{$bahan_baku->unit}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="3" style="text-align:center">Tidak ada bahan baku</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (session('status'))
        <div class="alert alert-success">
            <strong>{{ session('status') }}</strong>
        </div>
        @endif
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Bahan Baku</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Keluar/Masuk</th>
                    <th scope="col">Keterangan</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($reports->sortByDesc('created_at') as $report)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$report->bahan_baku->nama_bahan}}</td>
                    <td>{{$report->tanggal}}</td>
                    <td><strong> @if($report->keluar_masuk=='masuk') + @else - @endif{{$report->qty}}
                            {{$report->bahan_baku->unit}}</strong></td>
                    <td>
                        <div class="badge @if($report->keluar_masuk=='masuk') badge-primary @else badge-danger @endif">
                            {{$report->keluar_masuk}}</div>
                    </td>
                    <td>{{$report->keterangan}}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" style="text-align:center">Tidak ada catatan</td>
                </tr>
                @endforelse

            </tbody>
        </table>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>