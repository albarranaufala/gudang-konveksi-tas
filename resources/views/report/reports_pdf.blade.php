<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Report Tas</title>
    <style>
        table,
        th,
        td {
            padding: 10px;
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <div class="row my-3">
        <h1>LAPORAN KONVEKSI GUDANG TAS</h1>
    </div>
    <div class="row">
        <h3>Sisa bahan</h3>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Bahan</th>
                <th scope="col">Sisa</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($bahan_bakus as $bahan_baku)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$bahan_baku->nama_bahan}}</td>
                <td>{{$bahan_baku->qty}} {{$bahan_baku->unit}}</td>
            </tr>
            @empty
            <tr>
                <td colspan="3" style="text-align:center">Tidak ada bahan baku</td>
            </tr>
            @endforelse
        </tbody>
    </table>
    <br>
    <div class="row">
        <h3>Catatan Keluar Masuk</h4>
    </div>
    <table style="margin:auto" class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Bahan Baku</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Qty</th>
                <th scope="col">Keluar/Masuk</th>
                <th scope="col">Keterangan</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($reports->sortByDesc('created_at') as $report)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$report->bahan_baku->nama_bahan}}</td>
                <td>{{$report->tanggal}}</td>
                <td>@if($report->keluar_masuk=='masuk') + @else - @endif<strong>{{$report->qty}}
                        {{$report->bahan_baku->unit}}</strong></td>
                <td>
                    <div class="badge @if($report->keluar_masuk=='masuk') badge-primary @else badge-danger @endif">
                        {{$report->keluar_masuk}}</div>
                </td>
                <td>{{$report->keterangan}}</td>
            </tr>
            @empty
            <tr>
                <td colspan="6" style="text-align:center">Tidak ada catatan</td>
            </tr>
            @endforelse

        </tbody>
    </table>
</body>

</html>