<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jenis Tas</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <div class="container">
                <a class="navbar-brand" href="/">Gudang Konveksi Tas</a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/pemasukan/create">Input Bahan<span
                                class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/tas">Spesifikasi Tas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/pengeluaran/create">Produksi Tas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/report">Report</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <h1 class="my-3"><strong>{{$tas->nama_tas}}</strong></h1>
        <div class="row my-3">
            <a class="btn btn-link" href="/tas">kembali</a>
            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahJenisTas">Tambah
                Bahan</button>
            {{-- modal --}}
            <div class="modal fade" id="tambahJenisTas" tabindex="-1" role="dialog"
                aria-labelledby="tambahJenisTasTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="tambahJenisTasTitle">Tambah Bahan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="/bahan_tas/store" method="POST">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="nama_bahan">Nama Bahan</label>
                                    <select name="id_bahan_baku" class="form-control">
                                        @forelse ($bahan_bakus as $bahan_baku)
                                        <option value="{{$bahan_baku->id}}">{{$bahan_baku->nama_bahan}}</option>
                                        @empty

                                        @endforelse
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="qty">Kuantitas</label>
                                    <input name="qty" type="text" class="form-control" id="qty" placeholder="kuantitas">
                                </div>
                                <input hidden type="text" name="id_tas" value="{{$tas->id}}">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Bahan</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($tas->bahan_tas as $bahan_tas)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td><a>{{$bahan_tas->bahan_baku->nama_bahan}}</a></td>
                    <td><a>{{$bahan_tas->qty}} {{$bahan_tas->bahan_baku->unit}}</a></td>
                    <td>
                        <form action="/bahan_tas/{{$bahan_tas->id}}" method="POST">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger">hapus</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" style="text-align:center">Bahan baku belum ditambahkan</td>
                </tr>
                @endforelse

            </tbody>
        </table>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>