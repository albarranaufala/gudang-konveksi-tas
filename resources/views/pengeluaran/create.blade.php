<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Input Bahan Baku</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        html,
        body {
            height: 100%;
        }

        .full-height {
            height: 100%;
        }

        .center-page {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <div class="container">
                <a class="navbar-brand" href="/">Gudang Konveksi Tas</a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/pemasukan/create">Input Bahan<span
                                class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tas">Spesifikasi Tas</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/pengeluaran/create">Produksi Tas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/report">Report</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="full-height center-page">
        <div class="card col-md-6">
            <div class="card-body">
                <form action="/pengeluaran/store" method="POST">
                    @csrf
                    <h4 class="mb-3"><strong>Produksi Tas</strong></h4>
                    <div class="form-group">
                        <label for="jenisTas">Jenis Tas</label>
                        <select name="id_tas" class="form-control" id="jenisTas">
                            @forelse($tastas as $tas)
                            <option value="{{$tas->id}}">{{$tas->nama_tas}}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        @php
                        $month = date('m');
                        $day = date('d');
                        $year = date('Y');

                        $today = $year . '-' . $month . '-' . $day;
                        @endphp
                        <label for="tanggal">Tanggal Produksi</label>
                        <input name="tanggal" type="date" class="form-control" id="tanggal"
                            value="<?php echo $today; ?>">
                    </div>
                    <div class="form-group">
                        <label for="qty">Kuantitas</label>
                        <input name="qty" step="any" type="number" class="form-control" id="qty"
                            placeholder="Kuantitas">
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea name="keterangan" class="form-control" id="keterangan"
                            placeholder="Keterangan"></textarea>
                    </div>
                    <div class="form-row d-flex justify-content-between">
                        <a class="btn btn-link" href="/">kembali</a>
                        <button class="btn btn-primary" type="submit">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>

</html>