<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/pemasukan/create', 'PemasukanController@create');
Route::post('/pemasukan/store', 'PemasukanController@store');
Route::post('/jenis_bahan_baku/store', 'BahanBakuController@store');
//Tas
Route::get('/tas', 'TasController@index');
Route::post('/tas/store', 'TasController@store');
Route::get('/tas/show/{id}', 'TasController@show');
//Bahan Tas
Route::post('/bahan_tas/store', 'BahanTasController@store');
Route::delete('/bahan_tas/{id}', 'BahanTasController@destroy');
//Pengeluaran
Route::get('/pengeluaran/create', 'PengeluaranController@create');
Route::post('/pengeluaran/store', 'PengeluaranController@store');
//Report
Route::get('/report', 'ReportController@index');
Route::get('/report/download_pdf', 'ReportController@download_pdf');