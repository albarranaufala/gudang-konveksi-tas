<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BahanTas;

class BahanTasController extends Controller
{
    public function store(Request $request){
        $bahan_tas = new BahanTas();
        $bahan_tas->id_tas = $request->id_tas;
        $bahan_tas->id_bahan_baku = $request->id_bahan_baku;
        $bahan_tas->qty = $request->qty;
        $bahan_tas->save();
        return redirect("tas/show/$bahan_tas->id_tas");
    }
    public function destroy($id){
        $id_tas = BahanTas::find($id)->tas->id;
        BahanTas::destroy($id);
        return redirect("tas/show/$id_tas");
    }
}

