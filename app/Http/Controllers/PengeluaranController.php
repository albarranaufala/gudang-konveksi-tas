<?php

namespace App\Http\Controllers;

use App\Pengeluaran;
use App\JenisTas;
use App\BahanTas;
use App\Report;
use Illuminate\Http\Request;

class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tastas = JenisTas::all();
        return view('pengeluaran.create', compact('tastas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pengeluaran = new Pengeluaran();
        $pengeluaran->id_tas = $request->id_tas;
        $pengeluaran->tanggal = $request->tanggal;
        $pengeluaran->qty = $request->qty;
        $pengeluaran->keterangan = $request->keterangan;
        $tas = JenisTas::find($request->id_tas);
        $tidakbisa = false;
        foreach ($tas->bahan_tas as $bahan_tas) {
            $hasil = $bahan_tas->bahan_baku->qty - ($request->qty * $bahan_tas->qty);
            if($hasil<0){
                $tidakbisa = true;
            }
        }
        if($tidakbisa){
            $status = "Tidak bisa memproduksi, bahan baku kurang!";
        }else{
            foreach ($tas->bahan_tas as $bahan_tas) {
                $bahan_tas->bahan_baku->update([
                    'qty' => $bahan_tas->bahan_baku->qty - ($request->qty * $bahan_tas->qty)
                ]);
                $report = new Report();
                $report->id_bahan_baku = $bahan_tas->id_bahan_baku;
                $report->tanggal = $request->tanggal;
                $report->qty = $request->qty * $bahan_tas->qty;
                $ktr =  "Produksi tas " . $tas->nama_tas . " - " . $request->keterangan;
                $report->keterangan = $ktr;
                $report->keluar_masuk = "keluar";
                $report->save();
            }
    
            $pengeluaran->save();
            $status = "Berhasil memproduksi, bahan baku berkurang";
        }


        return redirect('/report')->with('status', $status);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pengeluaran  $pengeluaran
     * @return \Illuminate\Http\Response
     */
    public function show(Pengeluaran $pengeluaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pengeluaran  $pengeluaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengeluaran $pengeluaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pengeluaran  $pengeluaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pengeluaran $pengeluaran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pengeluaran  $pengeluaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengeluaran $pengeluaran)
    {
        //
    }
}
