<?php

namespace App\Http\Controllers;

use App\Pemasukan;
use App\BahanBaku;
use App\Report;
use Illuminate\Http\Request;

class PemasukanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bahan_bakus = BahanBaku::all();

        return view('pemasukan.create', compact('bahan_bakus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pemasukan = new Pemasukan();
        $pemasukan->id_bahan_baku = $request->id_bahan_baku;
        $pemasukan->tanggal = $request->tanggal;
        $pemasukan->qty = $request->qty;
        $pemasukan->keterangan = $request->keterangan;
        $bahan_baku = $pemasukan->bahan_baku;
        $bahan_baku->update([
            'qty' => $bahan_baku->qty+$pemasukan->qty
        ]);
        $pemasukan->save();

        $report = new Report();
        $report->id_bahan_baku = $request->id_bahan_baku;
        $report->tanggal = $request->tanggal;
        $report->qty = $request->qty;
        $report->keterangan = $request->keterangan;
        $report->keluar_masuk = "masuk";
        $report->save();
        
        return redirect('/report');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pemasukan  $pemasukan
     * @return \Illuminate\Http\Response
     */
    public function show(Pemasukan $pemasukan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pemasukan  $pemasukan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pemasukan $pemasukan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pemasukan  $pemasukan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pemasukan $pemasukan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pemasukan  $pemasukan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pemasukan $pemasukan)
    {
        //
    }
}
