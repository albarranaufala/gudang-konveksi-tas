<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahanTas extends Model
{
    protected $table = "bahan_untuk_tas";
    
    public function bahan_baku(){
        return $this->belongsTo('App\BahanBaku',  'id_bahan_baku');
    }
    public function tas(){
        return $this->belongsTo('App\JenisTas',  'id_tas');
    }
}
