<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = "catatan_keluar_masuk";

    public function bahan_baku(){
        return $this->belongsTo('App\BahanBaku',  'id_bahan_baku');
    }
}
