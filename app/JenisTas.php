<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisTas extends Model
{
    protected $table = "jenis_tas";

    public function bahan_tas(){
        return $this -> hasMany('App\BahanTas', 'id_tas');
    }
}
