<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    protected $table = "pengeluaran";

    public function jenis_tas(){
        return $this->belongsTo('App\JenisTas',  'id_tas');
    }
}
