<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahanBaku extends Model
{
    protected $table = "jenis_bahan_baku";
    protected $fillable = ['nama_bahan', 'qty', 'unit'];
}
