<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemasukan extends Model
{
    protected $table = "pemasukan";
    protected $fillable = ['id_bahan_baku', 'tanggal', 'qty', 'keterangan'];

    public function bahan_baku(){
        return $this->belongsTo('App\BahanBaku',  'id_bahan_baku');
    }
}
